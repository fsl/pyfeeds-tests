#!/usr/bin/env fslpython

import sys
import numpy as np
import nibabel as nib
import datetime
import pandas as pd

class S2VFeedsType(object):
    """ The purpose of this class is to make all the comparisons between
        estimated and ground truth eddy results for the Slice-to-vol
        feeds for eddy. There are some pretty hard assumptions of the
        data in here, so will need revision if we change the data for the test.
    """

    def __init__(self,mask,bvals,corr,truth,est_MOT,true_MOT,est_ol,true_ol):
        # Read corrected images and make sure dimensions are right
        try:
            self._mask = nib.load(mask,mmap=False)
            self._corr = nib.load(corr,mmap=False)
            self._truth = nib.load(truth,mmap=False)
            if not (all(self._mask.header['dim'][1:4] == self._corr.header['dim'][1:4]) and
                    all(self._mask.header['dim'][1:4] == self._truth.header['dim'][1:4])):
                raise Exception('S2VFeedsType:__init__:Size mismatch in first three dimensions of images')
            if not (self._mask.header['dim'][4] == 1 and
                    self._corr.header['dim'][4] == self._truth.header['dim'][4]):
                raise Exception('S2VFeedsType:__init__:Size mismatch in fourth dimension of images')
        except Exception as e:
            print(str(e))
            raise Exception('S2VFeedsType:__init__:Error opening image files')

        # Read bvals file
        try:
            self._bvals = pd.read_csv(bvals, delim_whitespace=True, header=None, skiprows=0).values[0]
            if self._bvals.shape[0] != self._corr.header['dim'][4]:
                raise Exception('S2VFeedsType:__init__:Size mismatch between bvals and images')
            b0index = (self._bvals < 100).nonzero()[0]
            lowbindex = np.logical_and([self._bvals > 100], [self._bvals < 1000]).nonzero()[1]
            hibindex = (self._bvals > 1000).nonzero()[0]
            self._indicies = [b0index, lowbindex, hibindex]
        except Exception as e:
            print(str(e))
            raise Exception('S2VFeedsType:__init__:Error opening bvals text file')

        # Read outlier text files
        try:
            self._est_ol = pd.read_csv(est_ol, delim_whitespace=True, header=None, skiprows=1).values
            self._true_ol = pd.read_csv(true_ol, sep=',', header=None, decimal='.').values
            if self._est_ol.shape[0] != self._corr.header['dim'][4] or \
               self._true_ol.shape[0] != self._corr.header['dim'][4]:
                raise Exception('S2VFeedsType:__init__:Size mismatch in volume dimension of outlier data')
            if self._est_ol.shape[1] != self._corr.header['dim'][3] or \
               self._true_ol.shape[1] != self._corr.header['dim'][3]:
                raise Exception('S2VFeedsType:__init__:Size mismatch in slice dimension of outlier data')
        except Exception as e:
            print(str(e))
            raise Exception('S2VFeedsType:__init__:Error opening outlier text files')

        # Read movement-over-time files
        try:
            self._est_MOT = pd.read_csv(est_MOT, delim_whitespace=True, header=None, skiprows=0).values
            self._true_MOT = pd.read_csv(true_MOT, delim_whitespace=True, header=None, skiprows=0).values
            if self._est_MOT.shape[1] != 6 or \
               self._true_MOT.shape[1] != 6:
                raise Exception('S2VFeedsType:__init__:Size mismatch in parameter dimension of MOT data')
            if self._est_MOT.shape[0] != np.prod(self._corr.header['dim'][3:5]) or \
               self._true_MOT.shape[0] != np.prod(self._corr.header['dim'][3:5]):
                raise Exception('S2VFeedsType:__init__:Size mismatch in time dimension of MOT data')
        except Exception as e:
            print(str(e))
            raise Exception('S2VFeedsType:__init__:Error opening movement over time text files')

        # Compare images and create statistics of differences
        try:
            mask = self._mask.get_fdata()
            mask = (mask > 0).astype(float)
            corrdiff = self._corr.get_fdata()
            corrdiff = abs(corrdiff - self._truth.get_fdata())

            self._corrdiffmeans = [np.zeros(len(self._indicies[0])), np.zeros(len(self._indicies[1])), np.zeros(len(self._indicies[2]))]
            for b in range(0,len(self._corrdiffmeans)):
                cntr = 0
                for vol in self._indicies[b]:
                    tmpdiff = np.multiply(mask, corrdiff[:, :, :, vol])
                    self._corrdiffmeans[b][cntr] = np.array(mask.shape).prod() * tmpdiff.mean() / mask.sum()
                    cntr += 1
        except Exception as e:
            print(str(e))
            raise Exception('S2VFeedsType:__init__:Error calculating image statistics')

        # Look extra carefully at the volumes that we know are affected by S2V movement
        # N.B. this will no longer be valid if we change the data we use for the tests
        #try:
        #    b0vols = [9, 27, 36, 45, 54, 63, 90, 99]
        #    lowbvols = [2, 4, 5, 6, 7, 8, 10, 11]
        #    hibvols = [41, 44, 57, 58, 79, 80, 89, 98, 100, 102, 103, 104, 106, 107]
        #    self._s2v_indicies = [b0vols, lowbvols, hibvols]
        #except Exception as e:
        #    print(str(e))
        #    raise Exception('S2VFeedsType:__init__:Error calculating image statistics for S2V affected volumes')

        # Calculate false positives, false negatives and true positives for outliers
        try:
            # Find slices with > 500 brain voxels
            vsl = []
            for sl in range(0,self._mask.header['dim'][3]):
                if mask[:,:,sl].sum() > 500:
                    vsl.append(sl)
            # Go through and count positives and false negatives
            self._tol = 0; self._tdol = 0; self._tp = 0; self._fp = 0; self._fn = 0
            for sl in vsl:
                for vol in range(0,self._true_ol.shape[0]):
                    if not np.isnan(self._true_ol[vol,sl]): # If it is a true outlier
                        self._tol += 1
                        if self._est_ol[vol,sl] == 1:
                            self._tp += 1
                        if self._true_ol[vol,sl] < 0.8: # If it ought to have been detected
                            self._tdol += 1
                            if self._est_ol[vol,sl] != 1:
                                self._fn += 1
                    else:
                        if self._est_ol[vol,sl] == 1:
                            self._fp += 1

        except Exception as e:
            print(str(e))
            raise Exception('S2VFeedsType:__init__:Error calculating outlier statistics')

        # Compare movement over time to ground truth
        try:
            # Map slices with > 500 brain voxels onto time-series
            index = []
            for vol in range(0,self._corr.header['dim'][4]):
                tmpindx = [x+vol*self._corr.header['dim'][3] for x in vsl]
                index.append(tmpindx)
            # Calculate differences in mm and degrees
            self._MOT_diffs = np.zeros((len(index)*len(index[0]),6))
            cntr = 0
            mmdeg = np.array([1, 1, 1, 180/np.pi, 180/np.pi, 180/np.pi])
            for vol in range(0,len(index)):
                for i in range(0,len(index[vol])):
                    self._MOT_diffs[cntr,:] = np.abs(mmdeg * (self._est_MOT[index[vol][i],:] - self._true_MOT[index[vol][i],:]))
                    cntr += 1
        except Exception as e:
            print(str(e))
            raise Exception('S2VFeedsType:__init__:Error calculating movement over time statistics')

    def BValues(self):
        return [0, 700, 2000]

    def MeanDiffsOfCorrectedImages(self):
        return self._corrdiffmeans

    def MeanDiffsOfCorrectedImages_b0(self):
        return self._corrdiffmeans[0]

    def MeanDiffsOfCorrectedImages_700(self):
        return self._corrdiffmeans[1]

    def MeanDiffsOfCorrectedImages_2000(self):
        return self._corrdiffmeans[2]

    def DiffsOfMovementOverTime(self):
        return self._MOT_diffs

    def MeanDiffsOfMovementOverTime(self):
        return self._MOT_diffs.mean(axis=0)

    def MaxDiffsOfMovementOverTime(self):
        return self._MOT_diffs.max(axis=0)

    def OlFalsePositives(self):
        return self._fp

    def OlFalseNegatives(self):
        return self._fn

    def OlTruePositives(self):
        return self._tp

    def OlTotalNumber(self):
        return self._tol

    def OlDetectableNumber(self):
        return self._tdol

def main(argv):
    # This is the main program that tests the output from running
    # the S2VFeeds tests.
    try:
        if len(argv) != 23:
            print('S2VFeeds.py usage: S2VFeeds output_dir prefix mask bvals corrected truth est_MOT true_MOT ' \
                  'est_ol true_ol allowed_mean_diff_b0 allowed_max_diff_b0 allowed_mean_diff_700 ' \
                  'allowed_max_diff_700 allowed_mean_diff_2000 allowed_max_diff_2000 allowed_false_pos ' \
                  'allowed_false_neg allowed_mean_trans_error allowed_max_trans_error allowed_mean_rot_error ' \
                  'allowed_max_rot_error')
            sys.exit(1)
        else:
            output_dir = argv[1]
            output_prefix = argv[2]
            mask = argv[3]
            bvals = argv[4]
            corrected = argv[5]
            truth = argv[6]
            est_MOT = argv[7]
            true_MOT = argv[8]
            est_ol = argv[9]
            true_ol = argv[10]
            allowed_mean_diff_b0 = float(argv[11])
            allowed_max_diff_b0 = float(argv[12])
            allowed_mean_diff_700 = float(argv[13])
            allowed_max_diff_700 = float(argv[14])
            allowed_mean_diff_2000 = float(argv[15])
            allowed_max_diff_2000 = float(argv[16])
            allowed_false_positives = float(argv[17])
            allowed_false_negatives = float(argv[18])
            allowed_mean_trans_error = float(argv[19])
            allowed_max_trans_error = float(argv[20])
            allowed_mean_rot_error = float(argv[21])
            allowed_max_rot_error = float(argv[22])


        # Try to create MoveBySuscFeedsType object (involves reading all files)
        try:
            s2vf = S2VFeedsType(mask,bvals,corrected,truth,est_MOT,true_MOT,est_ol,true_ol)
        except Exception as e:
            print(str(e))
            print('main: Error when creating S2VFeedsType object.')
            sys.exit(1)

        # Inspect statistics to see if it passes test
        try:
            # Image differences
            passes_test = True
            if s2vf.MeanDiffsOfCorrectedImages()[0].mean() > allowed_mean_diff_b0 or \
               s2vf.MeanDiffsOfCorrectedImages()[0].max() > allowed_max_diff_b0 or \
               s2vf.MeanDiffsOfCorrectedImages()[1].mean() > allowed_mean_diff_700 or \
               s2vf.MeanDiffsOfCorrectedImages()[1].max() > allowed_max_diff_700 or \
               s2vf.MeanDiffsOfCorrectedImages()[2].mean() > allowed_mean_diff_2000 or \
               s2vf.MeanDiffsOfCorrectedImages()[2].max() > allowed_max_diff_2000:
                passes_test = False
            # Outliers
            if s2vf.OlFalsePositives() > allowed_false_positives or \
               s2vf.OlFalseNegatives() > allowed_false_negatives:
                passes_test = False
            # Movement-over-time estimates
            if s2vf.MeanDiffsOfMovementOverTime()[0:3].mean() > allowed_mean_trans_error or \
               s2vf.MeanDiffsOfMovementOverTime()[3:6].mean() > allowed_mean_rot_error or \
               s2vf.MaxDiffsOfMovementOverTime()[0:3].mean() > allowed_max_trans_error or \
               s2vf.MaxDiffsOfMovementOverTime()[3:6].mean() > allowed_max_rot_error:
                passes_test = False
        except Exception as e:
            print(str(e))
            print('main: Failed calculating stats for test.')
            sys.exit(1)

        # Write report file
        try:
            fp = open(output_dir + '/' + output_prefix + '_S2VFeedsReport.txt','w')
        except Exception as e:
            print(str(e))
            print('Cannot open report file: ' + output_dir + '/' + output_prefix + '_S2VFeedsReport.txt')
            sys.exit(1)
        else:
            try:
                fp.write('S2VFeeds was run on ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + '\n')
                fp.write('With the command' + ' '.join(argv) + '\n')
                if passes_test:
                    fp.write('\nOverall the test passed\n')
                else:
                    fp.write('\nOverall the test failed\n')
                # Report on corrected image differences separately for b=0, 700 and 2000
                fp.write('\nThe absolute differences, averaged across the mask, ' \
                         'for the corrected b = {0:d} images were: '.format(s2vf.BValues()[0]) + \
                         ' '.join(["{0:.4f}".format(elem) for elem in s2vf.MeanDiffsOfCorrectedImages()[0]]) + '\n')
                fp.write('That gives mean error: ' + "{0:.4f}".format(s2vf.MeanDiffsOfCorrectedImages()[0].mean()) + \
                         ', and a max error: ' + "{0:.4f}".format(s2vf.MeanDiffsOfCorrectedImages()[0].max()) + '\n')
                fp.write('The allowed mean of averaged differences for the corrected b = {0:d} images was: {1:.4f}\n'\
                         .format(s2vf.BValues()[0],allowed_mean_diff_b0))
                fp.write('The allowed maximum of averaged differences for the corrected b = {0:d} images was: {1:.4f}\n'\
                         .format(s2vf.BValues()[0],allowed_max_diff_b0))
                if s2vf.MeanDiffsOfCorrectedImages()[0].mean() > allowed_mean_diff_b0 or \
                   s2vf.MeanDiffsOfCorrectedImages()[0].max() > allowed_max_diff_b0:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')

                fp.write('\nThe absolute differences, averaged across the mask, ' \
                         'for the corrected b = {0:d} images were: '.format(s2vf.BValues()[1]) + \
                         ' '.join(["{0:.4f}".format(elem) for elem in s2vf.MeanDiffsOfCorrectedImages()[1]]) + '\n')
                fp.write('That gives mean error: ' + "{0:.4f}".format(s2vf.MeanDiffsOfCorrectedImages()[1].mean()) + \
                         ', and a max error: ' + "{0:.4f}".format(s2vf.MeanDiffsOfCorrectedImages()[1].max()) + '\n')
                fp.write('The allowed mean of averaged differences for the corrected b = {0:d} images was: {1:.4f}\n'\
                         .format(s2vf.BValues()[1],allowed_mean_diff_700))
                fp.write('The allowed maximum of averaged differences for the corrected b = {0:d} images was: {1:.4f}\n'\
                         .format(s2vf.BValues()[1],allowed_max_diff_700))
                if s2vf.MeanDiffsOfCorrectedImages()[1].mean() > allowed_mean_diff_700 or \
                   s2vf.MeanDiffsOfCorrectedImages()[1].max() > allowed_max_diff_700:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')

                fp.write('\nThe absolute differences, averaged across the mask, ' \
                         'for the corrected b = {0:d} images were: '.format(s2vf.BValues()[2]) + \
                         ' '.join(["{0:.4f}".format(elem) for elem in s2vf.MeanDiffsOfCorrectedImages()[2]]) + '\n')
                fp.write('That gives mean error: ' + "{0:.4f}".format(s2vf.MeanDiffsOfCorrectedImages()[2].mean()) + \
                         ', and a max error: ' + "{0:.4f}".format(s2vf.MeanDiffsOfCorrectedImages()[2].max()) + '\n')
                fp.write('The allowed mean of averaged differences for the corrected b = {0:d} images was: {1:.4f}\n'\
                         .format(s2vf.BValues()[2],allowed_mean_diff_2000))
                fp.write('The allowed maximum of averaged differences for the corrected b = {0:d} images was: {1:.4f}\n'\
                         .format(s2vf.BValues()[2],allowed_max_diff_2000))
                if s2vf.MeanDiffsOfCorrectedImages()[2].mean() > allowed_mean_diff_2000 or \
                   s2vf.MeanDiffsOfCorrectedImages()[2].max() > allowed_max_diff_2000:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')

                # Report on differences in outlier estimations
                fp.write('\nThe input data had ' + '{:d}'.format(s2vf.OlTotalNumber()) + ' true outliers')
                fp.write('\nOf these ' + '{:d}'.format(s2vf.OlDetectableNumber()) + ' had a signal loss greater ' + \
                         'than 20% and should be detected')
                fp.write('\nThe analysis yielded ' + '{:d}'.format(s2vf.OlTruePositives()) + ' true positives, ' + \
                         '{:d}'.format(s2vf.OlFalseNegatives()) + ' false negatives and ' + \
                         '{:d}'.format(s2vf.OlFalsePositives()) + ' false positives\n')
                if s2vf.OlFalsePositives() > allowed_false_positives or \
                   s2vf.OlFalseNegatives() > allowed_false_negatives:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')
                # Report on differences in Movement over time
                fp.write('\nThe mean absolute errors in movement estimated over time were')
                fp.write('\nx-trans: {0:.4f} mm, y-trans: {1:.4f} mm, z-trans: {2:.4f} mm'.\
                         format(s2vf.MeanDiffsOfMovementOverTime()[0],s2vf.MeanDiffsOfMovementOverTime()[1],\
                         s2vf.MeanDiffsOfMovementOverTime()[2]))
                fp.write('\nx-rot: {0:.4f} deg, y-rot: {1:.4f} deg, z-rot: {2:.4f} deg'.\
                         format(s2vf.MeanDiffsOfMovementOverTime()[3],s2vf.MeanDiffsOfMovementOverTime()[4],\
                         s2vf.MeanDiffsOfMovementOverTime()[5]))
                fp.write('\nThe allowed mean absolute errors in movement estimated over time were')
                fp.write('Translation: {0:.4f} mm and Rotation: {1:.4f} deg\n'.format(allowed_mean_trans_error,allowed_mean_rot_error))
                if s2vf.MeanDiffsOfMovementOverTime()[0:3].mean() > allowed_mean_trans_error or \
                   s2vf.MeanDiffsOfMovementOverTime()[3:6].mean() > allowed_mean_rot_error:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')

                fp.write('\nThe max absolute errors in movement estimated over time were')
                fp.write('\nx-trans: {0:.4f} mm, y-trans: {1:.4f} mm, z-trans: {2:.4f} mm'.\
                         format(s2vf.MaxDiffsOfMovementOverTime()[0],s2vf.MaxDiffsOfMovementOverTime()[1],\
                         s2vf.MaxDiffsOfMovementOverTime()[2]))
                fp.write('\nx-rot: {0:.4f} deg, y-rot: {1:.4f} deg, z-rot: {2:.4f} deg'.\
                         format(s2vf.MaxDiffsOfMovementOverTime()[3],s2vf.MaxDiffsOfMovementOverTime()[4],\
                         s2vf.MaxDiffsOfMovementOverTime()[5]))
                fp.write('\nThe allowed mean absolute errors in movement estimated over time were')
                fp.write('Translation: {0:.4f} mm and Rotation: {1:.4f} deg\n'.format(allowed_max_trans_error,allowed_max_rot_error))
                if s2vf.MaxDiffsOfMovementOverTime()[0:3].mean() > allowed_max_trans_error or \
                   s2vf.MaxDiffsOfMovementOverTime()[3:6].mean() > allowed_max_rot_error:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')

                fp.close()
            except Exception as e:
                print(str(e))
                print('main: Problem writing report file: ' + output_dir + '/' + output_prefix + '_S2VFeedsReport.txt')
                sys.exit(1)

    except Exception as e:
        print(str(e))
        print('main: Unknown problem in body of function')
        sys.exit(1)


    # Exit with success or failure
    if passes_test:
        sys.exit(0)
    else:
        sys.exit(1)


if __name__ == "__main__":
    main(sys.argv)
