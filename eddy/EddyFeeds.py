#!/usr/bin/env fslpython

import sys
import numpy as np
import nibabel as nib
import datetime
import pandas as pd

class EddyFeedsType(object):
    """ The purpose of this class is to make all the comparisons between
        newly estimated and precomputed eddy results for plain vanilla and
        outlier eddy.
    """

    def __init__(self,mask,corr,precomp_corr,disp_field_base,precomp_disp_field_base,ol_txt,precomp_ol_txt):
        # Read corrected images and make sure dimensions are right
        try:
            self._mask = nib.load(mask,mmap=False)
            self._corr = nib.load(corr,mmap=False)
            self._precomp_corr = nib.load(precomp_corr,mmap=False)
        except Exception as e:
            print(str(e))
            raise Exception('EddyFeedsType:__init__:Error opening corrected image files')

        if not (all(self._mask.header['dim'][1:4] == self._corr.header['dim'][1:4]) and
                all(self._mask.header['dim'][1:4] == self._precomp_corr.header['dim'][1:4])):
            raise Exception('EddyFeeds:__init__:Size mismatch in first three dimensions of corrected images')
        if not (self._mask.header['dim'][4] == 1 and
                self._corr.header['dim'][4] == self._precomp_corr.header['dim'][4]):
            raise Exception('EddyFeedsType:__init__:Size mismatch in fourth dimension of corrected images')

        # Read displacement fields and make sure dimensions are right
        try:
            self._fields = []
            self._precomp_fields = []
            for vol in range(1,self._corr.header['dim'][4]+1):
                field_name = disp_field_base + ".{:03d}".format(vol)
                self._fields.append(self.OpenImageFile(field_name))
                precomp_field_name = precomp_disp_field_base + str(vol)
                self._precomp_fields.append(self.OpenImageFile(precomp_field_name))
        except Exception as e:
            print(str(e))
            raise Exception('EddyFeedsType:__init__:Error opening displacement field image files')

        for vol in range (0,self._corr.header['dim'][4]):
            if not (all(self._mask.header['dim'][1:4] == self._fields[vol].header['dim'][1:4]) and
                    all(self._mask.header['dim'][1:4] == self._precomp_fields[vol].header['dim'][1:4])):
                raise Exception('EddyFeedsType:__init__:Size mismatch in first three dimensions of displacement field image files')
            if self._fields[vol].header['dim'][4] != 3 or self._precomp_fields[vol].header['dim'][4] != 3:
                raise Exception('EddyFeedsType:__init__:Size mismatch in fourth dimension of displacement field image files')

        # Read outlier text files
        try:
            self._ol = pd.read_csv(ol_txt, delim_whitespace=True, header=None, skiprows=1).values
            self._precomp_ol = pd.read_csv(precomp_ol_txt, sep=',', header=None, decimal='.').values
            if self._ol.shape[0] != self._corr.header['dim'][4] or \
               self._precomp_ol.shape[0] != self._corr.header['dim'][4]:
                raise Exception('EddyFeedsType:__init__:Size mismatch in volume dimension of outlier data')
            if self._ol.shape[1] != self._corr.header['dim'][3] or \
               self._precomp_ol.shape[1] != self._corr.header['dim'][3]:
                raise Exception('EddyFeedsType:__init__:Size mismatch in slice dimension of outlier data')

        except Exception as e:
            print(str(e))
            raise Exception('EddyFeedsType:__init__:Error opening outlier text files')

        # Compare images and create statistics of differences
        try:
            mask = self._mask.get_fdata()
            mask = (mask > 0).astype(float)
            corrdiff = self._corr.get_fdata()
            corrdiff = abs(corrdiff - self._precomp_corr.get_fdata())

            self._corrdiffmeans = np.zeros(corrdiff.shape[3])
            for vol in range(0, corrdiff.shape[3]):
                tmpdiff = np.multiply(mask,corrdiff[:,:,:,vol])
                self._corrdiffmeans[vol] = np.array(mask.shape).prod() * tmpdiff.mean() / mask.sum()

            self._fielddiffmeans = np.zeros(len(self._fields))
            for vol in range(0,len(self._fields)):
                tmpdiff = self._fields[vol].get_fdata() - self._precomp_fields[vol].get_fdata()
                tmpdiff = np.square(tmpdiff[:,:,:,0]) + np.square(tmpdiff[:,:,:,1]) + np.square(tmpdiff[:,:,:,2])
                tmpdiff = np.multiply(mask,np.sqrt(tmpdiff))
                self._fielddiffmeans[vol] = np.array(mask.shape).prod() * tmpdiff.mean() / mask.sum()

        except Exception as e:
            print(str(e))
            raise Exception('EddyFeedsType:__init__:Error calculating image statistics')

        # Calculate false positives, false negatives and true positives for outliers
        try:
            # Find slices with > 500 brain voxels
            vsl = []
            for sl in range(0,self._mask.header['dim'][3]):
                if mask[:,:,sl].sum() > 500:
                    vsl.append(sl)
            # Go through and count positives and false negatives
            self._tol = 0; self._tdol = 0; self._tp = 0; self._fp = 0; self._fn = 0
            for sl in vsl:
                for vol in range(0,self._precomp_ol.shape[0]):
                    if not np.isnan(self._precomp_ol[vol,sl]): # If it is a true outlier
                        self._tol += 1
                        if self._ol[vol,sl] == 1:
                            self._tp += 1
                        if self._precomp_ol[vol,sl] < 0.8: # If it ought to have been detected
                            self._tdol += 1
                            if self._ol[vol,sl] != 1:
                                self._fn += 1
                    else:
                        if self._ol[vol,sl] == 1:
                            self._fp += 1

        except Exception as e:
            print(str(e))
            raise Exception('EddyFeedsType:__init__:Error calculating outlier statistics')

    def OpenImageFile(self,fname):
        # Try to open 'fname', 'fname'.nii and 'fname'.nii.gz in that order
        try:
            rval = nib.load(fname,mmap=False)
        except:
            try:
                rval = nib.load(fname+'.nii',mmap=False)
            except:
                try:
                    rval = nib.load(fname+'.nii.gz',mmap=False)
                except:
                    raise Exception('EddyFeedsType:OpenImageFile:Error opening file ' + fname)
        return rval

    def MeanDiffsOfCorrectedImages(self):
        return self._corrdiffmeans

    def MeanDiffsOfFields(self):
        return self._fielddiffmeans

    def OlFalsePositives(self):
        return self._fp

    def OlFalseNegatives(self):
        return self._fn

    def OlTruePositives(self):
        return self._tp

    def OlTotalNumber(self):
        return self._tol

    def OlDetectableNumber(self):
        return self._tdol

def main(argv):
    # This is the main program that tests the output from running
    # the eddy (vanilla and outliers) tests.
    try:
        if len(argv) != 16:
            print('EddyFeeds.py usage: EddyFeeds output_dir prefix mask corrected precomputed_corrected fields_basename ' \
                  'precomputed_fields_basename ol precomputed_ol allowed_mean_diff_corrected ' \
                  'allowed_max_diff_corrected allowed_mean_diff_field allowed_max_diff_field ' \
                  'allowed_false_positives allowed_false_negatives')
            sys.exit(1)
        else:
            output_dir = argv[1]
            output_prefix = argv[2]
            mask = argv[3]
            corrected = argv[4]
            precomputed_corrected = argv[5]
            fields_basename = argv[6]
            precomputed_fields_basename = argv[7]
            ol = argv[8]
            precomputed_ol = argv[9]
            allowed_mean_diff_corrected = float(argv[10])
            allowed_max_diff_corrected = float(argv[11])
            allowed_mean_diff_field = float(argv[12])
            allowed_max_diff_field = float(argv[13])
            allowed_false_positives = float(argv[14])
            allowed_false_negatives = float(argv[15])

        # Try to create EddyFeedsType object (involves reading all files)
        try:
            ef = EddyFeedsType(mask,corrected,precomputed_corrected,fields_basename, \
                               precomputed_fields_basename,ol,precomputed_ol)
        except Exception as e:
            print(str(e))
            print('main: Error when creating EddyFeedsType object.')
            sys.exit(1)

        try:
            passes_test = True
            # Check pass/fail based on corrected images and fields
            if ef.MeanDiffsOfCorrectedImages().mean() > allowed_mean_diff_corrected or \
               ef.MeanDiffsOfCorrectedImages().max() > allowed_max_diff_corrected or \
               ef.MeanDiffsOfFields().mean() > allowed_mean_diff_field or \
               ef.MeanDiffsOfFields().max() > allowed_max_diff_field:
                passes_test = False

            # Check pass/fail based on detected outliers
            if ef.OlFalsePositives() > allowed_false_positives or \
               ef.OlFalseNegatives() > allowed_false_negatives:
                passes_test = False
        except Exception as e:
            print(str(e))
            print('main: Failed calculating stats for test.')
            sys.exit(1)

        # Write report
        try:
            fp = open(output_dir + '/' + output_prefix + '_EddyReport.txt','w')
        except Exception as e:
            print(str(e))
            print('main: Cannot open report file: ' + output_dir + '/' + output_prefix + '_EddyReport.txt')
            sys.exit(1)
        else:
            try:
                fp.write('EddyFeeds was run on ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + '\n')
                fp.write('With the command' + ' '.join(argv) + '\n')
                if passes_test:
                    fp.write('\nOverall the test passed\n')
                else:
                    fp.write('\nOverall the test failed\n')
                # Report on differences in corrected images
                fp.write('\nThe absolute differences, averaged across the mask, for the corrected images were: ' + \
                         ' '.join(["{0:.4f}".format(elem) for elem in ef.MeanDiffsOfCorrectedImages()]) + '\n')
                fp.write('That gives mean error: ' + "{0:.4f}".format(ef.MeanDiffsOfCorrectedImages().mean()) + \
                         ', and a max error: ' + "{0:.4f}".format(ef.MeanDiffsOfCorrectedImages().max()) + '\n')
                fp.write('The allowed mean of averaged differences for the corrected images was: ' + \
                         "{0:.4f}".format(allowed_mean_diff_corrected) + '\n')
                fp.write('The allowed maximum of averaged differences for the corrected images was: ' + \
                         "{0:.4f}".format(allowed_max_diff_corrected) + '\n')
                if ef.MeanDiffsOfCorrectedImages().mean() > allowed_mean_diff_corrected or \
                   ef.MeanDiffsOfCorrectedImages().max() > allowed_max_diff_corrected:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')
                # Report on differences in estimated displacement fields
                fp.write('\nThe absolute differences, averaged across the mask, for the estimated displacement ' + \
                         'images were: ' + ' '.join(["{0:.4f}".format(elem) for elem in ef.MeanDiffsOfFields()]) + '\n')
                fp.write('That gives mean error: ' + "{0:.4f}".format(ef.MeanDiffsOfFields().mean()) + \
                         ', and a max error: ' + "{0:.4f}".format(ef.MeanDiffsOfFields().max()) + '\n')
                fp.write('The allowed mean of averaged differences for the estimated displacement images was: ' + \
                         "{0:.4f}".format(allowed_mean_diff_field) + '\n')
                fp.write('The allowed maximum of averaged differences for the estimated displacement images was: ' + \
                         "{0:.4f}".format(allowed_max_diff_field) + '\n')
                if ef.MeanDiffsOfFields().mean() > allowed_mean_diff_field or \
                   ef.MeanDiffsOfFields().max() > allowed_max_diff_field:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')
                # Report on differences in outlier estimations
                fp.write('\nThe input data had ' + '{:d}'.format(ef.OlTotalNumber()) + ' true outliers\n')
                fp.write('\nOf these ' + '{:d}'.format(ef.OlDetectableNumber()) + ' had a signal loss greater ' + \
                         'than 20% and should be detected\n')
                fp.write('\nThe analysis yielded ' + '{:d}'.format(ef.OlTruePositives()) + ' true positives, ' + \
                         '{:d}'.format(ef.OlFalseNegatives()) + ' false negatives and ' + \
                         '{:d}'.format(ef.OlFalsePositives()) + ' false positives\n')
                if ef.OlFalsePositives() > allowed_false_positives or \
                   ef.OlFalseNegatives() > allowed_false_negatives:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')
                fp.close()
            except Exception as e:
                print(str(e))
                print('main: Problem writing report file: ' + argv[1] + '/' + argv[2] + '_EddyReport.txt')
                sys.exit(1)
    except Exception as e:
        print(str(e))
        print('main: Unknown problem in body of function')
        sys.exit(1)

    if passes_test:
        sys.exit(0)
    else:
        sys.exit(1)


if __name__ == "__main__":
    main(sys.argv)
