#!/usr/bin/env fslpython

import sys
import numpy as np
import nibabel as nib
import datetime

class MoveBySuscFeedsType(object):
    """ The purpose of this class is to make all the comparisons between
        newly estimated and precomputed eddy results for the MoveBySusc
        feeds for eddy.
    """
    def __init__(self,mask,corr,deriv_fields,precomp_corr,precomp_deriv_fields):
        # Read image files and make sure dimensions are right
        try:
            self._mask = nib.load(mask,mmap=False)
            self._corr = nib.load(corr,mmap=False)
            self._deriv_fields = nib.load(deriv_fields,mmap=False)
            self._precomp_corr = nib.load(precomp_corr,mmap=False)
            self._precomp_deriv_fields = nib.load(precomp_deriv_fields,mmap=False)
        except Exception as e:
            print(str(e))
            raise Exception('MoveBySuscFeeds:__init__:Error opening image files')

        if not (all(self._mask.header['dim'][1:4]==self._corr.header['dim'][1:4]) and
                all(self._mask.header['dim'][1:4]==self._deriv_fields.header['dim'][1:4]) and
                all(self._mask.header['dim'][1:4]==self._precomp_corr.header['dim'][1:4]) and
                all(self._mask.header['dim'][1:4]==self._precomp_deriv_fields.header['dim'][1:4])):
            raise Exception('MoveBySuscFeeds:__init__:Size mismatch in first three dimensions')
        if not (self._mask.header['dim'][4] == 1 and
                self._corr.header['dim'][4] == self._precomp_corr.header['dim'][4] and
                self._deriv_fields.header['dim'][4] == self._precomp_deriv_fields.header['dim'][4]) :
            raise Exception('MoveBySuscFeeds:__init__:Size mismatch in fourth dimension')

        # Compare images and create statistics of differences
        try:
            mask = self._mask.get_fdata()
            mask = (mask > 0).astype(float)
            corrdiff = self._corr.get_fdata()
            corrdiff = abs(corrdiff - self._precomp_corr.get_fdata())
            fielddiff = self._deriv_fields.get_fdata()
            fielddiff = abs(fielddiff - self._precomp_deriv_fields.get_fdata())

            self._corrdiffmeans = np.zeros(corrdiff.shape[3])
            for vol in range(0, corrdiff.shape[3]):
                tmpdiff = np.multiply(mask,corrdiff[:,:,:,vol])
                self._corrdiffmeans[vol] = np.array(mask.shape).prod() * tmpdiff.mean() / mask.sum()

            self._fielddiffmeans = np.zeros(fielddiff.shape[3])
            for vol in range(0, fielddiff.shape[3]):
                tmpdiff = np.multiply(mask,fielddiff[:,:,:,vol])
                self._fielddiffmeans[vol] = np.array(mask.shape).prod() * tmpdiff.mean() / mask.sum()
        except Exception as e:
            print(str(e))
            raise Exception('MoveBySuscFeeds:__init__:Error calculating image statistics')

    def MeanDiffsOfCorrectedImages(self):
        return self._corrdiffmeans

    def MeanDiffsOfFieldDerivatives(self):
        return self._fielddiffmeans

def main(argv):
    # This is the main program that tests the output from running
    # the MoveBySuscFeeds tests.
    try:
        if len(argv) != 12:
            print('MoveBySuscFeeds.py usage: MoveBySuscFeeds output_dir prefix mask corrected precomputed_corrected fields precomputed_fields ' \
                  'allowed_mean_diff_corrected allowed_max_diff_corrected allowed_mean_diff_field allowed_max_diff_field')
            sys.exit(1)
        else:
            output_dir = argv[1]
            output_prefix = argv[2]
            mask = argv[3]
            corrected = argv[4]
            precomputed_corrected = argv[5]
            fields = argv[6]
            precomputed_fields = argv[7]
            allowed_mean_diff_corrected = float(argv[8])
            allowed_max_diff_corrected = float(argv[9])
            allowed_mean_diff_field = float(argv[10])
            allowed_max_diff_field = float(argv[11])

        # Try to create MoveBySuscFeedsType object (involves reading all files)
        try:
            mbsf = MoveBySuscFeedsType(mask,corrected,fields,precomputed_corrected,precomputed_fields)
        except Exception as e:
            print(str(e))
            print('main: Error when creating MoveBySuscFeedsType object.')
            sys.exit(1)

        # Compare stats to pre-defined limits and make a decision on pass or fail
        try:
            passes_test = True
            if mbsf.MeanDiffsOfCorrectedImages().mean() > allowed_mean_diff_corrected or \
               mbsf.MeanDiffsOfCorrectedImages().max() > allowed_max_diff_corrected or \
               mbsf.MeanDiffsOfFieldDerivatives().mean() > allowed_mean_diff_field or \
               mbsf.MeanDiffsOfFieldDerivatives().max() > allowed_max_diff_field:
                passes_test = False
        except Exception as e:
            print(str(e))
            print('main: Failed calculating stats for test.')
            sys.exit(1)

        # Write report
        try:
            fp = open(output_dir + '/' + output_prefix + '_MoveBySuscFeedsReport.txt','w')
        except Exception as e:
            print(str(e))
            print('main: Cannot open report file: ' + output_dir + '/' + output_prefix + '_MoveBySuscFeedsReport.txt')
            sys.exit(1)
        else:
            try:
                fp.write('MoveBySuscFeeds was run on ' + datetime.datetime.now().strftime("%Y-%m-%d %H:%M") + '\n')
                fp.write('With the command' + ' '.join(argv) + '\n')
                if passes_test:
                    fp.write('\nOverall the test passed\n')
                else:
                    fp.write('\nOverall the test failed\n')
                fp.write('\nThe absolute differences, averaged across the mask, for the corrected images were: ' + \
                         ' '.join(["{0:.4f}".format(elem) for elem in mbsf.MeanDiffsOfCorrectedImages()]) + '\n')
                fp.write('That gives mean error: ' + "{0:.4f}".format(mbsf.MeanDiffsOfCorrectedImages().mean()) + \
                         ', and a max error: ' + "{0:.4f}".format(mbsf.MeanDiffsOfCorrectedImages().max()) + '\n')
                fp.write('The allowed mean of averaged differences for the corrected images was: ' + \
                         "{0:.4f}".format(allowed_mean_diff_corrected) + '\n')
                fp.write('The allowed maximum of averaged differences for the corrected images was: ' + \
                         "{0:.4f}".format(allowed_max_diff_corrected) + '\n')
                if mbsf.MeanDiffsOfCorrectedImages().mean() > allowed_mean_diff_corrected or \
                   mbsf.MeanDiffsOfCorrectedImages().max() > allowed_max_diff_corrected:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')
                fp.write('\nThe absolute differences, averaged across the mask, for the estimated derivative images were: ' + \
                         ' '.join(["{0:.4f}".format(elem) for elem in mbsf.MeanDiffsOfFieldDerivatives()]) + '\n')
                fp.write('That gives mean error: ' + "{0:.4f}".format(mbsf.MeanDiffsOfFieldDerivatives().mean()) + \
                         ', and a max error: ' + "{0:.4f}".format(mbsf.MeanDiffsOfFieldDerivatives().max()) + '\n')
                fp.write('The allowed mean of averaged differences for the estimated derivative images was: ' + \
                         "{0:.4f}".format(allowed_mean_diff_field) + '\n')
                fp.write('The allowed maximum of averaged differences for the estimated derivative images was: ' + \
                         "{0:.4f}".format(allowed_max_diff_field) + '\n')
                if mbsf.MeanDiffsOfFieldDerivatives().mean() > allowed_mean_diff_field or \
                   mbsf.MeanDiffsOfFieldDerivatives().max() > allowed_max_diff_field:
                    fp.write('Based on these criteria the test failed\n')
                else:
                    fp.write('Based on these criteria the test passed\n')
                fp.close()
            except Exception as e:
                print(str(e))
                print('main: Problem writing report file: ' + output_dir + '/' + output_prefix + '_MoveBySuscFeedsReport.txt')
                sys.exit(1)

    except Exception as e:
        print(str(e))
        print('main: Unknown problem in body of function')
        sys.exit(1)

    # Exit with success or failure
    if passes_test:
        sys.exit(0)
    else:
        sys.exit(1)



if __name__ == "__main__":
    main(sys.argv)
