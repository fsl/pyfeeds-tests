This repository contains a set of tests which can be run via
[pyfeeds](https://git.fmrib.ox.ac.uk/fsl/pyfeeds/) to test an FSL
installation.


Note that many tests are dependent on various benchmark data sets which are
hosted internally at FMRIB, and thus cannot be executed outside of the FMRIB
computing system.


Note also that some tests (e.g. `fsl_course/fdt/bedpostx_gpu/`) need to be run
on a CUDA-capable machine.


Run the tests from the repository root as follows:

```
pyfeeds run                      \
    -c pyfeeds_config.cfg        \
    -i /path/to/input/data       \
    -b /path/to/benchmark/data   \
    -o /path/to/output_direcfory \
    [./paths/to/tests/you/want/to/run/ ...]
```
