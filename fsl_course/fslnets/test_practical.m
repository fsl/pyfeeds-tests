function test_practical( graphics )

    if nargin < 1, graphics = 0; end

    % Environment variables
    env_input   = getenv('FEEDS_FSLNETS_INPUT');  % path to ICA/Group folder
    env_output  = getenv('FEEDS_FSLNETS_OUTPUT'); % path to folder in which results should be saved

    assert(~isempty(env_input) && ~isempty(env_output), 'Environment variables are not set properly.' );

    % Setup paths
    path_here   = fileparts(mfilename('fullpath'));
    path_input  = env_input;
    path_output = env_output;

    %-----------------------------------


    % Load data from dual regression
    group_maps = fullfile(path_input,'groupmelodic60_fix.ica/melodic_IC');
    data_dir   = fullfile(path_input,'groupmelodic60_fix.ica/Age.DR');

    ts = nets_load( data_dir, 2, 1 );
    
    % Plot and check the spectra
    if graphics
        ts_spectra = nets_spectra(ts);
    end

    % Select "good" time-courses
    tsid_remove = [12 14 15 23 25 26 27 29 31 32 34 35 37 38 39 40 41 42 43 44 45 46 47 49 51 52 53 54 55 56 58 59 60];
    tsid_keep   = setdiff( 1:60, tsid_remove );

    ts.DD = tsid_keep;
    ts = nets_tsclean(ts,1); 


    % Compute netmats
    do_rtoz  = 0; % disable for testing
    netmats1 = nets_netmats(ts,do_rtoz,'corr');
    netmats2 = nets_netmats(ts,do_rtoz,'icov');
    netmats3 = nets_netmats(ts,do_rtoz,'ridgep',1);   
    
    save(fullfile(path_output,'netmats.matlab'),'netmats1','netmats2','netmats3');

    %-----------------------------------


    % Group average
    [Znet1,Mnet1] = nets_groupmean(netmats1,0);
    [Znet2,Mnet2] = nets_groupmean(netmats2,0);
    [Znet3,Mnet3] = nets_groupmean(netmats3,0);

    save(fullfile(path_output,'group.matlab'),'Znet1','Znet2','Znet3','Mnet1','Mnet2','Mnet3');
    # nets_netweb( Znet1, Znet2, ts.DD, group_maps, 'netweb' );

    % Visualise the hierarchy
    if graphics    
        
        % Using ward linkage
        nets_hierarchy(Znet1,Znet2,ts.DD,group_maps);
        
        % Or with netjs
        system('firefox netjs/index.html &');
    end

    %-----------------------------------


    % Univariate cross-subject modelling
    design   = fullfile( path_input, 'Designs/Age/design.mat' );
    contrast = fullfile( path_input, 'Designs/Age/design.con' );
    [p_uncorrected3,p_corrected3] = nets_glm(netmats3,design,contrast,graphics);
    
    save(fullfile(path_output,'glm.matlab'),'p_uncorrected3','p_corrected3');

    % Investigate the significant differences
    if graphics
        
        % Most significant edges
        nets_edgepics(ts,group_maps,Znet3,reshape(p_corrected3(4,:),ts.Nnodes,ts.Nnodes),6);
    
        % Visualise details of the t-test for particular edge
        nets_boxplots(ts,netmats3,23,7,10);
    end

    %-----------------------------------


    % Run LDA
    accuracy3_FLD = nets_lda(netmats3,10,2);
    accuracy3_SVM = nets_lda(netmats3,10,8);
    
    save(fullfile(path_output,'lda.matlab'),'accuracy3_FLD','accuracy3_SVM');

end
