function success = test_compare()

    % Environment variables
    path_fslnets = getenv('FEEDS_FSLNETS_ROOT');
    path_out     = getenv('FEEDS_FSLNETS_OUTPUT');
    path_ref     = fullfile(path_fslnets, 'test_data_ref' );
    
    assert( ~isempty(path_out), 'Environment variables are not set properly.' )

    % Define comparisons
    compare(1).file   = 'netmats';
    compare(1).names  = {'netmats1','netmats2','netmats3'};
    compare(1).thresh = 1e-12;

    compare(2).file   = 'group';
    compare(2).names  = {'Znet1','Znet2','Znet3'};
    compare(2).thresh = 1e-12;

    compare(3).file   = 'group';
    compare(3).names  = {'Mnet1','Mnet2','Mnet3'};
    compare(3).thresh = 1e-12;

    compare(4).file   = 'glm';
    compare(4).names  = {'p_uncorrected3','p_corrected3'};
    compare(4).thresh = 1e-12;

    compare(5).file   = 'lda';
    compare(5).names  = {'accuracy3_FLD','accuracy3_SVM'};
    compare(5).thresh = 1e-12;

    % Function used to compare arrays 
    difference = @(x,y) max(abs(x(:) - y(:)));

    % Iterate on comparisons
    success = true;
    ncomp   = numel(compare);
    for i = 1:ncomp

        % extract data
        comp   = compare(i);
        file   = comp.file;
        names  = comp.names;
        thresh = comp.thresh;

        fprintf( 'Comparison #%d (file %s.matlab)...\n', i, file );
 
        % load appropriate files
        reference = load(fullfile( path_ref, [file '.matlab'] ));
        target    = load(fullfile( path_out, [file '.matlab'] ));

        % compare each variables
        nvar = numel(names);
        err  = zeros(1,nvar);

        for j = 1:nvar            
            err(j) = difference( reference.(names{j}), target.(names{j}) );
            fprintf( '\t- Diff(%s) : %g\n', names{j}, err(j) );
        end

        % show status for this comparison
        status = {'FAIL','PASS'};
        nfail  = sum( err > thresh );
        status = status{ 1+(nfail==0) };
        fprintf( '\t+ Status: %s (%d/%d above %g)\n', status, nfail, nvar, thresh );

        % update overall status
        success = success && (nfail == 0); 

    end

end

