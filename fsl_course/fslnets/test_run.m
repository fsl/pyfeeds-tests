env_fsl     = getenv('FSLDIR');
env_fslnets = getenv('FEEDS_FSLNETS_ROOT');
env_libs = getenv('FEEDS_FSLNETS_OCTAVE_LIBS')

path_fsl     = fullfile( env_fsl,     'etc/matlab' );
path_fslnets = fullfile( env_fslnets);
path_stats   = fullfile( env_libs, 'statistics-1.2.4/inst' );
path_svm     = fullfile( env_libs, 'libsvm/matlab' );

addpath(path_fsl);
addpath(path_fslnets);
addpath(path_stats);
addpath(path_svm);

path

test_practical(0);

