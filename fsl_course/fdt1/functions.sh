#!/bin/bash
# Functions for FSL tests.

function testimage {
    image1=$1
    image2=$2
    diffimg=$3
    thres=$4

    fslmaths $image1 -sub $image2 $diffimg
    norm=`fslstats $image2 -a -m`
    error=`fslstats $diffimg -a -m`
    error=`echo $error / $norm | bc -l`

    if [ `echo "$thres < $error" | bc` -eq "1" ]; then
        echo "$0 failed on $image1: error = $error"
        exit 1
    fi 
}

function testVectorImage {
    image1=$1
    image2=$2
    diffimg=$3
    thres=$4
    numVectors=`fslstats $image2 -V` 
#TODO AWK num nonzero vecs
    fslmaths $image1 -mul $image2 -Tmean -mul 3 -abs $diffimg
    error=`fslstats $diffimg -l 0.99999 -V` 
#TODO num good vecs diff with above

    if [ `echo "$thres < $error" | bc` -eq "1" ]; then
        echo "$0 failed on $image1: error = $error"
        exit 1
    fi 
}

function testfile {
    file1=$1
    file2=$2

    value1=`md5sum $file1 | cut -d ' ' -f 1 | tr -d ' '`
    value2=`md5sum $file2 | cut -d ' ' -f 1 | tr -d ' '`

    if [ "$value1" != "$value1" ]; then
        echo "$0 failed on $file1"
        exit 1
    fi 
}
