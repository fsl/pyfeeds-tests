#!/bin/bash
# Test from the FSL course fdt1 practical.
# Tests tbss.

set -e

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt1/tbss

################
# tbss_1_preproc
################

# TBSS screws with the input files, so
# we'll make a copy of them, and move
# to that directory
mkdir $outdir/tbss
cp $subdatadir/*.nii.gz $outdir/tbss/
pushd $outdir/tbss > /dev/null

outdir=$outdir/tbss

# Build a list of all subjects
# (names of the input files)
subjects=""
for file in *.nii.gz; do
    subject=`basename $file .nii.gz`
    subjects="$subjects $subject"
done

tbss_1_preproc *.nii.gz

################
# tbss_2_preproc
################

# Skip this one - copy the pre-computed registrations
cp $subdatadir/precomputed_registration/* $outdir/FA/

################
# tbss_3_postreg
################

tbss_3_postreg -S

#################
# tbss_4_prestats
#################

tbss_4_prestats 0.3

###########
# Randomise
###########

randomise -i $outdir/stats/all_FA_skeletonised \
          -o $outdir/stats/tbss \
          -m $outdir/stats/mean_FA_skeleton_mask \
          -d $subdatadir/design.mat \
          -t $subdatadir/design.con \
          -c 1.5 \
          --seed=1

###########
# tbss_fill
###########

tbss_fill $outdir/stats/tbss_clustere_corrp_tstat1 0.949 \
          $outdir/stats/mean_FA \
          $outdir/stats/tbss_clustere_corrp_tstat1_filled

popd > /dev/null

exit 0
