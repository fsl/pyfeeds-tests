#!/bin/bash
# Test from the FSL course fdt1 practical.
# 
# Tests topup, eddy, and gps.

set -e
set -x

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt1/subj1_preproc

#######
# TOPUP
#######

# Extract a nodif image for the AP phase
# encoding direction, and merge this with
# the (pre-extracted) nodif_PA.
fslroi $subdatadir/dwidata $outdir/nodif 0 1
fslmerge -t $outdir/AP_PA_b0 $outdir/nodif $subdatadir/nodif_PA

# Run topup to estimate the distortion,
# and applytopup to correct all the
# diffusion images.

topup      --imain=$outdir/AP_PA_b0.nii.gz --datain=$subdatadir/acqparams.txt --config=b02b0.cnf --out=$outdir/topup_AP_PA_b0
applytopup --imain=$outdir/nodif,$subdatadir/nodif_PA --topup=$outdir/topup_AP_PA_b0 --datain=$subdatadir/acqparams.txt --inindex=1,2 --out=$outdir/hifi_nodif

######
# EDDY
######

# Create a brain mask, and test the output
bet $outdir/hifi_nodif $outdir/hifi_nodif_brain -m -f 0.2

# Correct for eddy currents, actually just make sure eddy ( both pre- and post- 5.0.9 naming scheme) runs and then simulate CTRL+C ( as per course instructions )
if which eddy_openmp &> /dev/null; then
    eddy_openmp --imain=$subdatadir/dwidata --mask=$outdir/hifi_nodif_brain_mask \
      --index=$subdatadir/index.txt --acqp=$subdatadir/acqparams.txt \
      --bvecs=$subdatadir/bvecs --bvals=$subdatadir/bvals \
      --fwhm=0 --topup=$outdir/topup_AP_PA_b0 --flm=quadratic \
      --out=$outdir/eddy_unwarped_images & sleep 10; kill -s TERM $!
elif which eddy &> /dev/null; then
    eddy --imain=$subdatadir/dwidata --mask=$outdir/hifi_nodif_brain_mask \
      --index=$subdatadir/index.txt --acqp=$subdatadir/acqparams.txt \
      --bvecs=$subdatadir/bvecs --bvals=$subdatadir/bvals \
      --fwhm=0 --topup=$outdir/topup_AP_PA_b0 --flm=quadratic \
      --out=$outdir/eddy_unwarped_image & sleep 10; kill -s TERM $!
fi

cp $subdatadir/pre_baked/eddy_unwarped_images* $outdir

# Finally, test the gps command, for generating bvecs
gps --ndir=64 --optws --out=$outdir/my.bvecs

exit 0
