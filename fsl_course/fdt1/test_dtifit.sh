#!/bin/bash
# Test from the FSL course fdt1 practical.
# Tests dtifit.

set -e

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt1/subj1

dtifit -k $subdatadir/data -m $subdatadir/nodif_brain_mask -r $subdatadir/bvecs -b $subdatadir/bvals -o $outdir/dti > /dev/null # standard output is not important

exit 0
