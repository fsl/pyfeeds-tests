#!/bin/bash -e
#
# A FEEDS test for melodic

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/rest/ICA/Group

unset SGE_ROOT

#Generate full paths to clean functionals, in standard space, for melodic
awk -v DATADIR="$subdatadir/" '{print DATADIR$0}' ${subdatadir}/inputlist_clean.txt > $outdir/inputlist_clean.txt

#Run melodic
melodic -i $outdir/inputlist_clean.txt -o $outdir/groupmelodic_fix.ica -d 25 -a concat --tr=2.000 --nobet --bgthreshold=10 -v --report --mmthresh=0.5 --Ostats --seed=1

echo "Finished group Melodic."

exit 0
