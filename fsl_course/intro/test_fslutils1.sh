#!/bin/bash
# Tests from the FSL course intro practical.
# Does a few basic tests on these commands:
# 
#   - fslinfo
#   - fslhd
#   - fslstats

set -e

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/intro

# First, we're going to run a bunch 
# of basic commands on three images.
testImages="highres thresh_zstat1 egepi"

# We run fslhd through tail,
# because it outputs the file
# name (as specified in the
# call) on the first line.
for image in $testImages; do
  fslinfo  $subdatadir/$image              > $outdir/fslinfo_${image}.output
  fslhd    $subdatadir/$image  | tail -n+2 > $outdir/fslhd_${image}.output
  fslstats $subdatadir/$image -R -r        > $outdir/fslstats_Rr_${image}.result
  fslstats $subdatadir/$image -l 1 -R -r   > $outdir/fslstats_l1Rr_${image}.result
  fslstats $subdatadir/$image -m -M        > $outdir/fslstats_mM_${image}.result
done

exit 0
