#!/bin/bash
# 
# Tests from the FSL course intro practical.
# Does a few basic fslmaths and fslstats tests.

set -e

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/intro

# Run some fslmaths commands
fslmaths $subdatadir/image0 -sub $subdatadir/image1 $outdir/imdiff
fslmaths $outdir/imdiff -div $subdatadir/image0 -mul 100 $outdir/imdiffpercent
fslmaths $subdatadir/LThal_mask_func -thr 0.5 -bin $outdir/LThal_mask_func_bin 

# Test fslstats on the above images.
# Using md5sum for an exact match
fslstats $outdir/LThal_mask_func_bin -V                              > $outdir/fslstats_LThal_mask_1.result
fslstats $subdatadir/thresh_zstat1 -k $outdir/LThal_mask_func_bin -V > $outdir/fslstats_LThal_mask_2.result
fslstats $subdatadir/thresh_zstat1 -k $outdir/LThal_mask_func_bin -M > $outdir/fslstats_LThal_mask_3.result

exit 0
