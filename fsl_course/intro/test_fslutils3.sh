#!/bin/bash
# Tests from the FSL course intro practical.
# Tests the fslsplit and fslmerge cmmands.

set -e

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/intro

# Split a diffusion image (assumed to be 64 volumes)
fslsplit $subdatadir/diffdata $outdir/vol

# Rename the nodif volumes
mv $outdir/vol0000.nii.gz $outdir/b0vol0000.nii.gz
mv $outdir/vol0016.nii.gz $outdir/b0vol0016.nii.gz
mv $outdir/vol0032.nii.gz $outdir/b0vol0032.nii.gz
mv $outdir/vol0048.nii.gz $outdir/b0vol0048.nii.gz

# Merge the diffusion and nodif volumes.
# Be specific on the globbing, because
# testimage will have created vol*_error
# and b0vol*_error images.
fslmerge -t $outdir/dwidata    $outdir/vol????.*
fslmerge -t $outdir/nodiffdata $outdir/b0vol????.*

exit 0
