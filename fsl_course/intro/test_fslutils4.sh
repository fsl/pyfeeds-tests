#!/bin/bash
# Tests from the FSL course intro practical.
# Tests the fslroi command.

set -e

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/intro

# Do some fslroi-ing
fslroi $subdatadir/wrapped $outdir/back  0 256 70 186 0 128
fslroi $subdatadir/wrapped $outdir/front 0 256 0  70  0 128
fslmerge -y $outdir/fix_wrapped $outdir/back.nii.gz $outdir/front.nii.gz
fslroi $subdatadir/highres $outdir/highres_roi `fslstats $subdatadir/highres -w`

# Save the output of fslhd too, to
# make sure fslroi sets affines
# correctly.
# Crop the first line of fslhd,
# because it prints the file name,
# which may not match.
fslhd $outdir/back        | tail -n+2 > $outdir/fslhd_back.output
fslhd $outdir/front       | tail -n+2 > $outdir/fslhd_front.output
fslhd $outdir/fix_wrapped | tail -n+2 > $outdir/fslhd_fix_wrapped.output
fslhd $outdir/highres_roi | tail -n+2 > $outdir/fslhd_highres_roi.output

exit 0

