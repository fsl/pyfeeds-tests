#!/bin/bash
# Test from the FSL course intro practical.
# Tests a few instantiations of BET, and
# robustfov.

set -e

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/intro

bet $subdatadir/structural $outdir/structural_brain       -f 0.5 -g 0 -m -s
bet $subdatadir/structural $outdir/structural_brain_lowf  -f 0.2
bet $subdatadir/structural $outdir/structural_brain_highf -f 0.8


bet $subdatadir/sub3m0 $outdir/sub3m0_brain
bet $subdatadir/sub3m0 $outdir/sub3m0_brain_g0.5      -g 0.5
bet $subdatadir/sub3m0 $outdir/sub3m0_brain_g0.2_f0.3 -g 0.2 -f 0.3

bet $subdatadir/bighead   $outdir/bighead_brain
bet $subdatadir/bighead   $outdir/bighead_brain_R          -R
bet $subdatadir/bighead   $outdir/bighead_brain_c89_97_161 -c 89 97 161

robustfov -i $subdatadir/bighead -r $outdir/bighead_crop > $outdir/robustfov_bighead.output
fslhd $outdir/bighead_crop | tail -n+2 > $outdir/fslhd_bighead_crop.output
bet $outdir/bighead_crop  $outdir/bighead_crop_brain

exit 0
