#!/bin/bash -e
#
# A FEEDS test which checks the dyads registration to standard space (vecreg).
# The test will check that the warped orientations will keep 
# their final orientations.

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt2

# Registering dyads1 from native diffusion space to MNI152 standard space
vecreg -i ${subdatadir}/subj1_2fibres.bedpostX/dyads1 -o ${outdir}/dyads1_to_standard -r ${subdatadir}/subj1_2fibres.bedpostX/standard -w ${subdatadir}/subj1_2fibres.bedpostX/xfms/diff2standard_warp

# Need to compute the cosine of the angle between the old and the newly conmputed orientations in standard space
#benchmark=${subdatadir}/subj1_2fibres.bedpostX/dyads1_to_standard
#fslmaths $benchmark -mul $benchmark -Tmean -mul 3 -abs ${outdir}/absCos_self
#fslmaths $benchmark -mul ${outdir}/dyads1_to_standard -Tmean -mul 3 -abs ${outdir}/absCos
#fslmaths ${outdir}/absCos_self -sub ${outdir}/absCos ${outdir}/diff
#UPDATE - Not sure the below code calculates the right error - should just be diff/absCos_self? Moving to python anyway

# Calculate the mean error (normalised) between
# the output and the benchmark
#norm=`fslstats $benchmark -a -m`
#error=`fslstats $outdir/diff -a -m`
#error=`echo $error / $norm | bc -l`

exit 0


