#!/bin/bash -e
#
# A FEEDS test which runs find_the_biggest on a set of tracking results
# to parcellate the right thalamus based on structural connectivity information

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt2

# Run find_the_biggest

export LC_COLLATE=C 
find_the_biggest ${subdatadir}/subj1.bedpostX/THAL2CTX_right/seeds_to_* ${outdir}/biggest_segmentation

exit 0


