#!/bin/bash -e
#
# A FEEDS test which runs probabilistic tracking from a seed voxel
# in MNI152 T1 space. To allow comparison between results and
# benchmark, the random seed is set to 10

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt2

# Tracking from voxels in MNI152 standard space
probtrackx2 --simple --seedref=${subdatadir}/subj1.bedpostX/standard.nii.gz -o singleSeedMNITract -x ${subdatadir}/singleSeedMNITract/fdt_coordinates.txt  -l --onewaycondition -c 0.2 -S 2000 --steplength=0.5 -P 5000 --fibthresh=0.01 --distthresh=0.0 --sampvox=0.0 --xfm=${subdatadir}/subj1.bedpostX/xfms/standard2diff_warp.nii.gz --invxfm=${subdatadir}/subj1.bedpostX/xfms/diff2standard_warp.nii.gz --forcedir --opd -s ${subdatadir}/subj1.bedpostX/merged -m ${subdatadir}/subj1.bedpostX/nodif_brain_mask  --dir=${outdir}/singleSeedMNITract --rseed=10

exit 0


