#!/bin/bash -e
#
# A FEEDS test which runs probabilistic tracking from a seed mask (left LGN)
# defined in MNI152 T1 standard space. The example uses anatomical priors (waypoint, exclusion and
# termination masks). To allow comparison between results and
# benchmark, the random seed is set to 10

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt2

# Tracking from voxels in MNI152 standard space
probtrackx2  -x ${subdatadir}/subj1_2fibres.bedpostX/SEED_LGN_LEFT.nii.gz  -l --onewaycondition -c 0.2 -S 2000 --steplength=0.5 -P 5000 --fibthresh=0.01 --distthresh=0.0 --sampvox=0.0 --xfm=${subdatadir}/subj1_2fibres.bedpostX/xfms/standard2diff_warp.nii.gz --invxfm=${subdatadir}/subj1_2fibres.bedpostX/xfms/diff2standard_warp.nii.gz --avoid=${subdatadir}/subj1_2fibres.bedpostX/EXCLUSION.nii.gz --stop=${subdatadir}/subj1_2fibres.bedpostX/TARGET_V1_LEFT.nii.gz --forcedir --opd -s ${subdatadir}/subj1_2fibres.bedpostX/merged -m ${subdatadir}/subj1_2fibres.bedpostX/nodif_brain_mask  --dir=${outdir}/LGNTracts --waypoints=${subdatadir}/subj1_2fibres.bedpostX/TARGET_V1_LEFT.nii.gz  --waycond=AND --rseed=10

exit 0


