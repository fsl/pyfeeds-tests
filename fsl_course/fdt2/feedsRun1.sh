#!/bin/bash -e
#
# A FEEDS test which runs probabilistic tracking from a seed voxel
# in native diffusion space. To allow comparison between results and
# benchmark, the random seed is set to 10

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt2

# Simple tracking - connectivity from a single seed point in the internal capsule
probtrackx2 --simple --seedref=${subdatadir}/subj1.bedpostX/nodif_brain_mask -o singleSeedTract -x ${subdatadir}/singleSeedTract/fdt_coordinates.txt  -l --onewaycondition -c 0.2 -S 2000 --steplength=0.5 -P 5000 --fibthresh=0.01 --distthresh=0.0 --sampvox=0.0 --forcedir --opd -s ${subdatadir}/subj1.bedpostX/merged -m ${subdatadir}/subj1.bedpostX/nodif_brain_mask --dir=${outdir}/singleSeedTract --rseed=10

exit 0


