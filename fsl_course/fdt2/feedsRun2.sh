#!/bin/bash -e
#
# A FEEDS test which runs probabilistic tracking from a seed voxel
# in subject native T1 space. To allow comparison between results and
# benchmark, the random seed is set to 10

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/fdt2

# Tracking from a voxel in non diffusion space
probtrackx2 --simple --seedref=${subdatadir}/subj1.bedpostX/struct_brain.nii.gz -o singleSeedStructTract -x ${subdatadir}/singleSeedStructTract/fdt_coordinates.txt  -l --onewaycondition -c 0.2 -S 2000 --steplength=0.5 -P 5000 --fibthresh=0.01 --distthresh=0.0 --sampvox=0.0 --xfm=${subdatadir}/subj1.bedpostX/xfms/str2diff.mat --forcedir --opd -s ${subdatadir}/subj1.bedpostX/merged -m ${subdatadir}/subj1.bedpostX/nodif_brain_mask  --dir=${outdir}/singleSeedStructTract --rseed=10

exit 0


