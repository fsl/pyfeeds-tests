#!/bin/bash -e
#
# A FEEDS test for dual_regression

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/rest/ICA/Group

unset SGE_ROOT

#Generate full paths to clean functionals, in standard space, for melodic, taking only even numbered subj - still gives balances design
awk -v DATADIR="$subdatadir/" 'NR % 2 == 0 {print DATADIR$0}'  ${subdatadir}/inputlist_clean.txt > $outdir/inputlist_clean.txt
# Run the dual regression
# Magic numbers in command:
# 1: variance normalise; -1: don't supply design files; 0: don't run randomise
echo "Running dual regression..."

#dual_regression "${inputICA}/melodic_IC.nii.gz" 1 -1 0 "$outputDR" `cat inputlist_clean.txt`
dual_regression ${subdatadir}/groupmelodic_fix.ica/melodic_IC.nii.gz 1 `pwd`/design.mat `pwd`/design.con 500 ${outdir}/Age.DR `cat ${outdir}/inputlist_clean.txt`

echo "Done."

exit 0
