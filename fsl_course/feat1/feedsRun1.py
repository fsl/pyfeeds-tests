#!/usr/bin/env fslpython
#
# A FEEDS test which runs FEAT, as per fsl course FEAT1 prac, and then
# checks the output

import sys
import os
import os.path as op
import glob
import util
import numpy as np

# parse args

outDir = sys.argv[1]
dataDir = sys.argv[2]
fslDir = os.environ['FSLDIR']

# copy FSF to test location and update paths

origFeatDir = op.join(dataDir, "fsl_course_data/fmri/fmri_fluency")
origFSF = op.join(os.getcwd(), "design.fsf")
origFSLDir = "/opt/fmrib/fsl-alpha"
origDataDir = "/vols/Data/fsldev/dataSets"

newFSF = op.join(outDir, "design.fsf")

cmd = "./cleanFSF.py {0} {1} {2} {3} {4} {5}".format(
    origFSF, newFSF, origDataDir, dataDir, outDir, origFSLDir)

util.run(cmd)

# run FEAT with new FSF

util.run("unset SGE_ROOT; feat {0}".format(newFSF))

sys.exit(0)
