#!/usr/bin/env fslpython
#
# A FEEDS test which runs renderhighres, as per fsl course FEAT1 prac, and then
# checks the output

import sys
import os
import os.path as op
import glob
import util

# parse args

outDir = sys.argv[1]
dataDir = sys.argv[2]
fslDir = os.environ['FSLDIR']

# run renderhighres

origFeatDir = op.join(dataDir, "fsl_course_data/fmri/fmri_fluency/fmri.feat")
newFeatDir = op.join(outDir, op.basename(origFeatDir))

util.run("renderhighres {0} standard highres 1 1 15".format(newFeatDir))

sys.exit(0)
