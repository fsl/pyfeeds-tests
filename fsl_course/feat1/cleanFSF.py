#!/usr/bin/env fslpython
import os
import sys


inputfile=sys.argv[1]
outputfile=sys.argv[2]
olddatadir=os.path.join(sys.argv[3],'')
currentdatadir=os.path.join(sys.argv[4], '')
outputdir=os.path.join(sys.argv[5], '')
oldfsldir=os.path.join(sys.argv[6], '')
currentfsldir=os.path.join(os.environ["FSLDIR"],'')
with open(inputfile, "r") as input, open(outputfile,"w") as output:
    for line in input:
        if olddatadir in line:
            line=line.replace(olddatadir,currentdatadir)
        if "fmri(outputdir)" in line:
            tokens=line.split()
            oldoutputdir=os.path.dirname(tokens[2])
            oldoutputdir=oldoutputdir.strip('"')
            print (oldoutputdir)
            if oldoutputdir != '':
                oldoutputdir=os.path.join(oldoutputdir,'')
                line=line.replace(oldoutputdir,outputdir)
            print(line)
        if oldfsldir in line:
            line=line.replace(oldfsldir,currentfsldir)
        output.write(line)
