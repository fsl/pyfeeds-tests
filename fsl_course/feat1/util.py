#!/usr/bin/env fslpython
#
# Some useful utils for feedsRuns

from subprocess import check_output
import nibabel as nb
import numpy as np
import hashlib
import sys


def calcHash(value):

    hashObj = hashlib.md5()
    hashObj.update(value)
    return hashObj.hexdigest()


def hashCompare(file1, file2):

    hash1 = calcHash(open(file1, 'rb').read())
    hash2 = calcHash(open(file2, 'rb').read())
    return (hash1 == hash2)


def normImageError(im1, im2):

    im1data = nb.load(im1).get_data()
    im2data = nb.load(im2).get_data()

    return np.abs((im1data-im2data)).mean() / np.abs(im1data).mean()


def normError(arr1, arr2):
    return np.abs((arr1-arr2)).mean() / np.abs(arr1).mean()


def run(cmd):
    print("RUNNING: " + cmd)
    check_output(cmd, shell=True)


def evalError(err, strict):
    if (err < 0.01):
        print(": error= {0:.4f} : PASS (err < 1%)".format(err))
    elif not strict and (err < 0.1):
        print(": error= {0:.4f} : PASS (err < 10%)".format(err))
    else:
        print(": error= {0:.4f} : FAIL".format(err))
        sys.exit(1)
