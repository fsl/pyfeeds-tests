#!/usr/bin/env fslpython
#
# A FEEDS test which runs featquery, as per fsl course FEAT1 prac, and then
# checks the output

import sys
import os
import os.path as op
import glob
import util
import numpy as np


# parse args

outDir = sys.argv[1]
dataDir = sys.argv[2]
fslDir = os.environ['FSLDIR']

# run featquery with Broca ROI

origFeatDir = op.join(dataDir, "fsl_course_data/fmri/fmri_fluency/fmri.feat")
newFeatDir = op.join(outDir, op.basename(origFeatDir))

util.run("imcp {0}/fsl_course_data/fmri/fmri_fluency/Broca "
         "{1}/Broca".format(dataDir, outDir))
util.run("unset SGE_ROOT; featquery 1 {0} 12  stats/cope1 stats/cope2 "
         "stats/cope5 stats/tstat1 stats/tstat2 stats/tstat5 stats/zstat1 "
         "stats/zstat2 stats/zstat5 thresh_zstat1 thresh_zstat2 thresh_zstat5 "
         "featquery -a 9 -p -s -w {1}/Broca".format(newFeatDir, outDir))

# run featquery with atlas ROI
atlas = op.join(fslDir,"data/atlases/HarvardOxford/HarvardOxford-cort-prob-2mm")
tmpfile = op.join(outDir, "featquery_atlas_roi")

util.run("fslroi {0} {1} 12 1".format(atlas, tmpfile))
util.run("unset SGE_ROOT; featquery 1 {0} 12  stats/cope1 stats/cope2 "
         "stats/cope5 stats/tstat1 stats/tstat2 stats/tstat5 stats/zstat1 "
         "stats/zstat2 stats/zstat5 thresh_zstat1 thresh_zstat2 thresh_zstat5 "
         "featquery -a 9 -p -s -w {1}".format(newFeatDir, tmpfile))

sys.exit(0)
