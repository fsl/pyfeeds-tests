#!/usr/bin/env fslpython
#
# A FEEDS test which runs FEAT and then checks the output

import sys
import os
import os.path as op
from subprocess import check_output
import glob
import nibabel as nb
import numpy as np
import hashlib
import re
import shutil

def run(cmd):
    print("RUNNING: " + cmd)
    check_output(cmd, shell=True)

# parse args

outDir = sys.argv[1]
dataDir = sys.argv[2]
fslDir = os.environ['FSLDIR']

# copy FSF to test location and update paths

origFeatDir = op.join(dataDir, "fsl_course_data/fmri/fluency2/fmri.feat")
origFSF = op.join(origFeatDir, "design_new.fsf")
origFSLDir = "/opt/fmrib/fsl-alpha"
origDataDir = "/vols/Data/fsldev/dataSets"

newFSF = op.join(outDir, "design.fsf")
newFeatDir = op.join(outDir, op.basename(origFeatDir))

cmd = "./cleanFSF.py {0} {1} {2} {3} {4} {5}".format(
    origFSF, newFSF, origDataDir, dataDir, outDir, origFSLDir)
run(cmd)

#The output FEAT directory will be the copied input, so update fsf
text = open(newFSF, 'rt').read()
text = re.sub('set feat_files\(1\).*',  f'set feat_files(1) "{newFeatDir}"', text)
open(newFSF, 'wt').write(text)

# run FEAT
shutil.copytree(origFeatDir,newFeatDir)
run("chmod -R a+w {0}".format(newFeatDir))
run("unset SGE_ROOT; feat {0}".format(op.join(outDir, "design.fsf")))

sys.exit(0)
