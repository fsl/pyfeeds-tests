#!/usr/bin/env fslpython
#
# A FEEDS test which runs FEAT and then checks the output

import sys
import os
import os.path as op
import utils

# parse args

outDir = sys.argv[1]
dataDir = sys.argv[2]
fslDir = os.environ['FSLDIR']

# copy FSF to test location and update paths
origFSF = op.join(dataDir, "fsl_course_data/fmri/bf/design.fsf")
origFSLDir = "/opt/fmrib/fsl-alpha"
origDataDir = "/vols/Data/fsldev/dataSets"

newFSF = op.join(outDir, "design.fsf")

cmd = "./cleanFSF.py {0} {1} {2} {3} {4} {5}".format(origFSF, newFSF, origDataDir, dataDir, outDir, origFSLDir)
utils.run(cmd)

# run FEAT
utils.run("unset SGE_ROOT; feat {0}".format(op.join(outDir, "design.fsf")))

sys.exit(0)
