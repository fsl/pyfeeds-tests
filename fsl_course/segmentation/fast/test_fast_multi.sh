#!/bin/bash -e
#
# Runs multi-channel FAST, and tests all outputs

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/seg_struc/fast

# Run BET on the t1 and t2,
# and register the t2 to the t1.
bet $subdatadir/sub2_t1.nii.gz $outdir/sub2_t1_brain.nii.gz
bet $subdatadir/sub2_t2.nii.gz $outdir/sub2_t2_brain.nii.gz

flirt -in  $outdir/sub2_t2_brain.nii.gz \
      -ref $outdir/sub2_t1_brain.nii.gz \
      -out $outdir/sub2_t2_to_t1 \
      -omat $outdir/sub2_t2_to_t1.mat \
      -bins 256 -cost corratio \
      -searchrx 0 0 -searchry 0 0 -searchrz 0 0 \
      -dof 6  -interp trilinear

# Run multi-channel FAST
prefix=sub2_t1_brain
fast -S 2 -n 4 -H 0.1 -I 4 -l 20.0  -o $outdir/$prefix $outdir/$prefix $outdir/sub2_t2_to_t1.nii.gz

exit 0