#!/bin/bash -e
#
# Runs FAST, and tests all outputs

outdir=$1
datadir=$2
subdatadir=$datadir/fsl_course_data/seg_struc/fast

# Run BET, and extract a few slices
bet    $subdatadir/structural.nii.gz           $outdir/structural_cod_brain.nii.gz
fslroi $outdir/structural_cod_brain.nii.gz $outdir/structural_cod_brain_roi.nii.gz 55 150 15 200 65 5

# Run FAST on those slices
prefix=structural_cod_brain_roi

fast -t 1 -n 3 -H 0.1 -I 4 -l 20.0 -b -o $outdir/$prefix $outdir/$prefix

# Do the same for the pre-generated
# slices with a different bias field
prefix=structural_brain_roi_inhomog

fast -t 1 -n 3 -H 0.1 -I 10 -l 20.0 -b -o $outdir/$prefix $subdatadir/$prefix

exit 0
