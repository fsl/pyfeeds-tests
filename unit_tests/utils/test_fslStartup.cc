#include <iostream>
#include "omp.h"
#include "cblas.h"

#include "utils/options.h"

using namespace std;

/*
 * Interrogate and print out the number of threads used by OpenMP, and the
 * number of threads used by OpenBLAS.
 *
 * The utils/fslStartup.cc file contains some global initialisation logic
 * which controls the number of threads used by OpenBLAS.  All we need to do
 * to induce the fslStartup.cc code is link against libfsl-utils.so.
 */
int main(int argc, char *argv[]) {

    // Use something from libfsl-utils.so
    // to ensure that it gets linked.
    Utilities::OptionParser opts("test", "test");
    opts.usage();

    int omp_threads;
    int blas_threads;
    int sum[16];

    for (int i = 0; i < 16; i++) {
      sum[i] = 0;
    }

    // omp num threads should not be affected
    // by the FSL startup logic.  Sum should
    // be equal to omp num threads.
    #pragma omp parallel
    {
        sum[omp_get_thread_num()] = 1;
        omp_threads               = omp_get_num_threads();
    }

    // blas num threads should be controlled
    // by FSL startup logic.
    blas_threads = openblas_get_num_threads();

    for (int i = 1; i < 16; i++) {
      sum[0] += sum[i];
    }

    cout << omp_threads << " " << blas_threads << " " << sum[0] << endl;
}
